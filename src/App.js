import React, { Component } from 'react'
import 'antd/dist/antd.css'
import { Button } from 'antd'
// import AuthModal from './AuthModal'
import DocRenderer from './DocRenderer'
import TransformTool from './TransformTool'
import MessageBox from './MessageBox'
import Nodes from './Nodes'
import isOpen from './kolo/state/isOpen'

class App extends Component {
  render () {
    return (
      <div className='App'>
        <DocRenderer />
        <MessageBox />
        <TransformTool />
        <Button
          onClick={() => isOpen.open()}
          style={{ bottom: 4, left: 4, position: 'fixed' }}
        >
          nodes
        </Button>
        <Nodes />
      </div>
    )
  }
}

export default App
