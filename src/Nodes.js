// @flow
/* global PIXI */
import React from 'react'
import client from './client'
import createListener from './twins/createListener'
import Web from './kolo/components/Web'
import isOpen from './kolo/state/isOpen'
import { Button } from 'antd'

export default class TarnsformTool extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      doc: null
    }
  }
  componentDidMount () {
    client.addListener(createListener(
      q => q.find({type: 'Doc'}),
      doc => doc && this.setState({doc}),
    ))
    isOpen.addListener(() => this.forceUpdate())
  }
  render () {
    const { doc } = this.state

    if (!doc || !isOpen.value) {
      return <div hidden />
    }

    return (
      <div>
        <Web idRootSource={doc.id} client={client} />
      </div>
    )
  }
}
