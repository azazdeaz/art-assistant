
module.exports.init1 = [
  {
    'type': 'Doc',
    'layers': [
      '#41093bc7-9b57-40bf-b243-653a5548bc8f',
      '#256f48c5-816c-482d-87de-f4fbcb21acb3',
      '#f227bcda-9b38-4b29-a386-e11ffde05c95'
    ],
    'messages': [],
    'selectedLayer': null,
    'id': '#37578a00-53e8-4e21-a8c6-52a33f071eda'
  },
  {
    'type': 'Transform',
    'x': 0,
    'y': 0,
    'scaleX': 1,
    'scaleY': 1,
    'skewX': 0,
    'skewY': 0,
    'pivotX': 0,
    'pivotY': 0,
    'rotation': 0,
    'id': '#126db9c9-b307-4679-92ab-734ecb5c9289'
  },
  {
    'type': 'Shape',
    'width': 100,
    'height': 100,
    'color': 16724259,
    'shape': 'rectangle',
    'transform': '#126db9c9-b307-4679-92ab-734ecb5c9289',
    'id': '#41093bc7-9b57-40bf-b243-653a5548bc8f'
  },
  {
    'type': 'Transform',
    'x': 123,
    'y': 0,
    'scaleX': 0.2,
    'scaleY': 0.2,
    'skewX': 0,
    'skewY': 0,
    'pivotX': 0,
    'pivotY': 0,
    'rotation': 0,
    'id': '#f5aeb49a-0d83-4202-9907-d9219302e0d1'
  },
  {
    'type': 'Sprite',
    'source': 'pe.png',
    'transform': '#f5aeb49a-0d83-4202-9907-d9219302e0d1',
    'id': '#256f48c5-816c-482d-87de-f4fbcb21acb3'
  },
  {
    'type': 'Transform',
    'x': 0,
    'y': 0,
    'scaleX': 1,
    'scaleY': 1,
    'skewX': 0,
    'skewY': 0,
    'pivotX': 0,
    'pivotY': 0,
    'rotation': 0,
    'id': '#ae548b69-f0c3-4aed-ade7-fb3d0d5a3308'
  },
  {
    'type': 'Text',
    'transform': '#ae548b69-f0c3-4aed-ade7-fb3d0d5a3308',
    'text': 'Text',
    'fill': 0,
    'id': '#f227bcda-9b38-4b29-a386-e11ffde05c95'
  }
]
