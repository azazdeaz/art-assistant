// @flow

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const http = require('http').createServer(app)
require('./wsServer').default(http)
app.use(bodyParser.json())

// import User from './rdbModels/User'

app.use(express.static('./build'))

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

// app.get('/createUser/:name/:email', async (req, res) => {
//   console.log(req.params)
//   const user = await new User({
//     name: req.params.name,
//     email: req.params.email,
//   }).save()
//
//   res.json({id: user.id})
// });
app.get('/test', async (req, res) => {
  res.json({test: true})
})

// app.post('/login', async (req, res) => {
//   const {username} = req.body
//   console.log({body: req.body})
//   const user = await User
//     .filter({name: username})
//     .nth(0).default(null)
//     .run()
//
//   if (user) {
//     return res.json({token: user.id})
//   }
//   res.json({error: `Can't find user with the name ${username}`})
// });

http.listen(process.env.PORT_HTTP_SERVER)
