import thinky from 'thinky'

export default thinky({
  host: process.env.RDB_HOST,
  port: process.env.RDB_PORT,
  db: process.env.RDB_DB
})
