// @flow

const socket_io = require('socket.io')
import createServer from '../twins/createServer'
import createClient from '../twins/createClient'
import createListener from '../twins/createListener'
import * as exec from '../twins/exec'
import createDrivers from '../drivers/createDrivers'
import apiai from 'apiai'
import parseColor from 'parse-color'
import testSources from './_testStartSources'

const nlu = apiai('56211d6795534c5aa53b89504a6cffdf')

const tsServer = createServer(testSources.init1)

const botClient = createClient()
botClient.drivers = createDrivers(botClient)
botClient.connectServer()
const { UserMessage, Shape } = botClient.drivers

function handleMsg (msg) {
  var request = nlu.textRequest(msg, {sessionId: '1'})

  request.on('response', function (response) {
    console.log(response)
    const doc = botClient.q.find({type: 'Doc'})
    const {color = 'red', shape = 'rectangle'} = response.result.parameters
    const options = {
      width: 33,
      height: 33,
      shape
    }
    if (color) {
      options.color = parseInt(parseColor(color).hex.slice(1), 16)
    }
    if (shape) {
      options.shape = shape
    }
    const _shape = Shape.create(options)
    botClient.addAction(exec.push(doc.id, 'layers', _shape.id))
  })

  request.on('error', function (error) {
    console.log(error)
  })

  request.end()
}

botClient.addListener(createListener(
  q => q.find({type: 'Doc'}),
  doc => {
    if (doc && doc.messages) {
      doc.messages.some(idMessage => {
        const message = botClient.q.get(idMessage)
        console.log('message>>>>>', message, UserMessage.type)
        if (message.type === UserMessage.type && !message.processed) {
          botClient.addAction(exec.set(idMessage, 'processed', true))
          const action = exec.insert({
            type: 'BotMessage',
            text: 'yo!'
          })
          botClient.addAction(action)
          // const msg = BotMessage.create('yo!')
          botClient.addAction(exec.push(doc.id, 'messages', action.params[0].id))
          handleMsg(message.text)

          return true
        }
        return false
      })
    }
  }
))

export default function (http: Object) {
  const io = socket_io(http)

  io.on('connection', function (socket) {
    const connection = tsServer.createConnection(
      state => socket.emit('sendState', state),
      (actions, idOverrideUntil) => socket.emit('sendActions', {actions, idOverrideUntil}),
    )
    socket.on('addAction', action => connection.addAction(action))

    socket.on('disconnect', () => connection.dispose())
  })
}
