// @flow

import thinky from '../thinky'
const { type } = thinky

const User = thinky.createModel('User', {
  id: type.string(),
  name: type.string().required(),
  email: type.string().email().required(),
})

User.ensureIndex('name')
User.ensureIndex('email')

export default User
