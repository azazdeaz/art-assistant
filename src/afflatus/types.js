// @flow

export type Driver = {
  getSource: () => Object,
  setSource: Object => void,
}

export type DriverMap = Map<string, Driver>

export type MrA = {
  getSource: () => Object,
  setSource: Object => void,
}
