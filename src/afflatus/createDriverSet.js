// @flow

import type { Driver } from './types'

function createMrA (
  driverMap,
) {
  const drivedSources = new Map()
  let rootDriver

  function createDriverFactory (sources) {
    function driverFactory (idSource: string) {
      if (drivedSources.has(idSource)) {
        return driverMap.get(rootSource.type).create(rootSource)
      }

      const source = sources.find(({id}) => id === idSource)
      driverMap.get(source.type).create(source, driverFactory)
    }

    return driverFactory
  }

  const mra = {
    createDriver (type: string, description: Object) {
      driverMap.set(type, description)
    },
    setSource (sources, idRoute) {
      rootDriver = createDriverFactory(sources)(idRoute)
    },
    getSource () {
      rootDriver.getSource()
    }
  }
}

export default function (version = '1.0.0') {
  const set = []
  const addDriver = driver => set.push(driver)

  return {
    version,
    set,
    addDriver
  }
}

// function createDriver(name) {
//   const driver = {_name: name}
//   driver.apply = value => driver.value = value
//   driver.simpleValue = (name) => {
//     driver[name]
//   }
// }
