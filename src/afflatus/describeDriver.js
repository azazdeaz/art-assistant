function describeDriver () {
  const driver = {}
  let val = null

  const driverEditors = {

  }

  const valEditors = {

  }

  const p = new Proxy({}, {
    get (_, name) {
      if (val) {
        if (valEditors[name]) {
          return valEditors[name]
        }
        val = null
      }
      if (driverEditors[name]) {
        return driverEditors[name]
      }

      throw Error(`Unknown command "${name}"`)
    }
  })
}
