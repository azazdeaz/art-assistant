// @flow
import createListenable from '../utils/createListenable'

export default createListenable(1)
