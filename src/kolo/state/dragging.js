// @flow
import pull from 'lodash/pull'

let to = null
let from = null
const listeners = []

function notifyListeners () {
  listeners.forEach(listener => listener())
}

function addListener (listener) {
  listeners.push(listener)
  return () => pull(listeners, listener)
}

function reset () {
  to = null
  from = null
  notifyListeners()
}

function start (target) {
  from = target
  notifyListeners()
}
function setTarget (target) {
  to = target
}

const dragging = {
  start,
  setTarget,
  get from () { return from },
  get to () { return to },
  get a () { return from },
  get b () { return to },
  get isDragging () { return Boolean(from) },
  reset,
  addListener
}
global.dragging = dragging // DEBUG
export default dragging
