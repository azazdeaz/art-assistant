// @flow

const positions = new Map()
import pull from 'lodash/pull'

const createRecord = () => {
  let x = 0
  let y = 0
  const listeners = []
  function set (_x: number, _y: number) {
    if (x !== _x || y !== _y) {
      x = _x
      y = _y
      listeners.forEach(listener => listener(x, y))
    }
  }

  function addListener (listener: Function) {
    listeners.push(listener)
    return () => pull(listeners, listener)
  }

  return {
    get x () { return x },
    get y () { return y },
    set x (_x) { set(_x, y) },
    set y (_y) { set(x, _y) },
    set,
    addListener
  }
}

export default function position (id) {
  if (!positions.has(id)) {
    positions.set(id, createRecord())
  }
  return positions.get(id)
}
