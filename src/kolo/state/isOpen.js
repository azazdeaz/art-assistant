// @flow

import createListenable from '../utils/createListenable'
const isOpen = createListenable(false)
isOpen.open = () => isOpen.value = true
isOpen.close = () => isOpen.value = false

export default isOpen
