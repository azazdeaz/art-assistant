// @flow
import React from 'react'
import positions from '../state/positions'
import getPositionID from '../utils/getPositionID'
import getColor from '../utils/getColor'

const styles = {
  base: {
    position: 'absolute',
    overflow: 'visible',
    top: 0,
    left: 0,
    pointerEvents: 'none'
  }
}

class Wire extends React.Component {
  shouldComponentUpdate () {
    return false
  }
  componentWillMount () {
    this.updatePositions()
  }
  componentWillReceiveProps (nextProps) {
    const { start, end } = this.props
    if (start !== nextProps.start || end !== nextProps.end) {
      this.updatePositions()
    }
  }
  updatePositions () {
    const { start, end } = this.props
    console.log('Wire up', start, end)
    if (start && end) {
      this.disposePositions()
      this.posStart = positions(getPositionID(start))
      this.posEnd = positions(getPositionID(end))
      this.disposeFrom = this.posStart.addListener(() => {
        this.forceUpdate()
      })
      this.disposeTo = this.posEnd.addListener(() => {
        this.forceUpdate()
      })
    }
  }
  disposePositions () {
    if (this.disposeFrom) {
      this.disposeFrom()
    }
    if (this.disposeTo) {
      this.disposeTo()
    }
  }
  componentWillUnmount () {
    this.disposeFrom()
    this.disposeTo()
  }

  render () {
    const { posStart, posEnd } = this
    if (!posStart) {
      return <div hidden />
    }
    // console.log(`wire ${this.props.start.toString()} -> ${this.props.end.toString()}`)

    const xDistance = Math.abs(posEnd.x - posStart.x)
    const handleSize = Math.min(xDistance / 3, 84)
    const d = `M${posStart.x},${posStart.y}`
      + ` C${posStart.x + handleSize},${posStart.y}`
      + ` ${posEnd.x - handleSize},${posEnd.y}`
      + ` ${posEnd.x},${posEnd.y}`

    return (
      <svg style={styles.base}>
        <path d={d}
          style={{
            stroke: getColor(getPositionID(this.props.start)),
            strokeWidth: 2,
            fill: 'none'
          }}
        />
      </svg>
    )
  }
}

export default Wire
