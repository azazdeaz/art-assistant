// @flow
import React from 'react'
import Plug from './Plug'
import Input from '../../matter/components/Input'
import Label from '../../matter/components/Label'
// import {Input} from 'antd'

const styles = {
  base: {
    display: 'flex'
  }
}

class Option extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      value: props.prop.value
    }
  }
  componentDidMount () {
    const { prop } = this.props
    this.dispose = prop.addListener(() => {
      console.log('catch change', prop.value)
      this.setState({ value: prop.value })
    })
  }
  componentWillUnmount () {
    this.dispose()
  }
  render () {
    const { prop } = this.props

    return (
      <div
        style={styles.base}
        onMouseDown={e => e.stopPropagation()}
      >
        <Plug target={prop} />
        {(prop.is('number') || prop.is('string'))
          ? (
            <Input
              size='small'
              value={prop.value}
              type={prop.type}
              onChange={value => {
                prop.value = value
              }}
            />
          )
          : prop.isComplex
          ? <Label label={prop.name} />
          : null
        }
      </div>
    )
  }
}

export default Option
