// @flow
import React from 'react'
import zoom from '../state/zoom'
import isOpen from '../state/isOpen'
import { Button, Slider } from 'antd'

export default class Controls extends React.Component {
  componentDidMount () {
    this.dispose = zoom.addListener(() => this.forceUpdate())
  }

  componentWillUnmount () {
    this.dispose()
  }

  render () {
    return (
      <div
        style={{ bottom: 4, left: 4, position: 'fixed', display: 'flex' }}
        onMouseDown={e => e.stopPropagation()}
      >
        <Button onClick={() => isOpen.close()}>
          close
        </Button>
        <div style={{width: 200}}>
          <Slider
            step={0.1}
            min={0.1}
            max={2}
            value={zoom.value}
            onChange={value => zoom.value = value}
          />
        </div>
      </div>
    )
  }
}
