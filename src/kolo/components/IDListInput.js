// @flow
import React from 'react'
import Label from '../../matter/components/Label'
import Plug from './Plug'

class IDListInput extends React.Component {
  render () {
    const { prop } = this.props

    return (
      <div>
        <div style={{ display: 'flex' }}>
          <Plug target={prop} />
        </div>
        {prop.value.map(id => (
          <div key={id} style={{ display: 'flex' }}>
            <Label label={`${id}`} />
          </div>
        ))}
      </div>
    )
  }
}

export default IDListInput
