// @flow
import React from 'react'
import Wire from './Wire'
import dragging from '../state/dragging'
import positions from '../state/positions'
import getPositionID from '../utils/getPositionID'

class DraggedWire extends React.Component {
  componentDidMount () {
    this.dispose = dragging.addListener(() => {
      this.forceUpdate()
    })
  }

  componentWillUnmount () {
    this.dispose()
  }

  render () {
    console.log('render dragging', dragging.isDragging, dragging.from && positions(getPositionID(dragging.from)))
    return dragging.isDragging
      ? (
        <Wire
          key={getPositionID(dragging.from)}
          start={dragging.from}
          end='->'
        />
      )
      : <div hidden />
  }
}

export default DraggedWire
