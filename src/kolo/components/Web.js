// @flow
import React from 'react'
import Box from './Box'
import Wire from './Wire'
import customDrag from '../../customDrag'
import positions from '../state/positions'
import zoom from '../state/zoom'
import DraggedWire from './DraggedWire'
import Controls from './Controls'

const styles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    backgroundColor: `rgba(123,123,123,.3)`
  }
}

const dragOptions = {
  onDown (props, monitor, component) {
    monitor.setData({
      initX: component.state.xOffset,
      initY: component.state.yOffset
    })
  },
  onDrag (props, monitor, component) {
    const offset = monitor.getDifferenceFromInitialOffset()

    component.setState({
      xOffset: monitor.data.initX + offset.x,
      yOffset: monitor.data.initY + offset.y
    })
  }
}

class Web extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      xOffset: 0,
      yOffset: 0
    }
  }
  componentDidMount () {
    this.dispose = this.props.client.addListener(() => {
      this.forceUpdate()
    })
    this.disposeZoom = zoom.addListener(() => {
      this.forceUpdate()
    })
  }
  componentWillUnmount () {
    this.dispose()
    this.disposeZoom()
  }
  getChildContext () {
    return {
      webOffset: {
        x: this.state.xOffset,
        y: this.state.yOffset
      }
    }
  }

  render () {
    const { idRootSource, dragRef, client } = this.props
    const { xOffset, yOffset } = this.state
    const boxes = []
    const wires = []

    const renderBoxes = (driver, dPrev) => {
      if (boxes.find(box => box.driver === driver)) {
        return
      }

      console.log(`render box ${driver}`)
      const position = positions(driver.id)

      if (position.x === 0) {
        if (dPrev) {
          const posPrev = positions(dPrev.id)
          position.x = posPrev.x - 120
        } else {
          position.x = 800
        }
      }
      if (position.y === 0) {
        position.y = Math.random() * 600
      }

      boxes.push({ driver, position })

      for (let propName in driver.props) {
        const prop = driver.props[propName]
        if (prop.isComplex && prop.value) {
          if (prop.isArray) {
            prop.value.forEach(listItem => {
              renderBoxes(listItem, driver)
              wires.push({
                start: listItem,
                end: prop,
                key: `${driver.id}-${prop.name}-${listItem.id}`
              })
            })
          } else {
            renderBoxes(prop.value, driver)
            wires.push({
              start: prop.value,
              end: prop,
              key: `${driver.id}-${prop.name}-${prop.value.id}`
            })
          }
        }
      }
    }

    renderBoxes(client.getDriver(idRootSource))

    const offsetTransform = [
      `translate(${xOffset}px, ${yOffset}px)`,
      `scale(${zoom.value})`
    ].join(' ')

    return (
      <div>
        <div style={styles.overlay} ref={dragRef}>
          <div style={{
            transform: offsetTransform,
            transformOrigin: 'center center'
          }}>
            {wires.map(wire => (
              <Wire
                key={wire.key}
                start={wire.start}
                end={wire.end}
              />
            ))}
            {boxes.map(box => (
              <Box
                key={`box-${box.driver.id}`}
                driver={box.driver}
                position={box.position}
              />
            ))}
            <DraggedWire />
          </div>
        </div>
        <Controls />
      </div>
    )
  }
}

Web.childContextTypes = {
  webOffset: React.PropTypes.shape({
    x: React.PropTypes.number,
    y: React.PropTypes.number
  })
}

const dragHoC = customDrag(dragOptions, connect => ({
  dragRef: connect.getDragRef()
}))

export default dragHoC(Web)
