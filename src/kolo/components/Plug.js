// @flow
import React from 'react'
import positions from '../state/positions'
import dragging from '../state/dragging'
import getPositionID from '../utils/getPositionID'
import customDrag from '../../customDrag'

const styles = {
  base: {
    width: 10,
    height: 10,
    backgroundColor: 'white',
    borderRadius: 5,
    boxSizig: 'border-box',
    margin: '6px'
  }
}

function updateCursorPosition (monitor) {
  const { x, y } = monitor.getClientOffset()
  positions('->').set(x, y)
}

const dragOptions = {
  onDown (props, monitor) {
    console.log('start drag', props.target)
    dragging.start(props.target)
    updateCursorPosition(monitor)
  },
  onDrag (props, monitor) {
    updateCursorPosition(monitor)
  },
  onLeave (props) {
    if (dragging.isDragging) {
      dragging.setTarget(null)
    }
  },
  onUp (props) {
    const { a, b } = dragging
    if (a && b !== a) {
      const prop = a.isProp ? a : b.isProp ? b : null
      const driver = a.isDriver ? a : b.isDriver ? b : null

      if (prop.isComplex) {
        prop.value = driver.id

        if (prop.isArray) {
          prop.value.push(driver.id)
        }
      }
    }
    dragging.reset()
  }
}

class Plug extends React.Component {
  componentDidMount () {
    this.registerPosition()
  }

  componentDidUpdate () {
    this.registerPosition()
  }

  registerPosition () {
    const { webOffset } = this.context
    const br = this.node.getBoundingClientRect()
    positions(getPositionID(this.props.target)).set(
      br.left - webOffset.x,
      br.top - webOffset.y,
    )
  }

  render () {
    return (
      <div
        onMouseEnter={() => {
          if (dragging.isDragging) {
            console.log('over target', this.props.target)
            dragging.setTarget(this.props.target)
          }
        }}
        style={{ ...styles.base, ...this.props.stype }}
        ref={_node => {
          this.node = _node
          this.props.dragRef(_node)
        }}
      />
    )
  }
}

Plug.porpTypes = {
  target: React.PropTypes.object.isRequired
}

Plug.contextTypes = {
  webOffset: React.PropTypes.shape({
    x: React.PropTypes.number,
    y: React.PropTypes.number
  })
}

const dragHoC = customDrag(dragOptions, connect => ({
  dragRef: connect.getDragRef()
}))

export default dragHoC(Plug)
