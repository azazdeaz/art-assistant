// @flow
import React from 'react'
import customDrag from '../../customDrag'
import Option from './Option'
import IDListInput from './IDListInput'
import Plug from './Plug'
import Panel from '../../matter/components/Panel'
import Label from '../../matter/components/Label'

const styles = {
  base: {
    position: 'absolute',
    width: 160
  }
}

const dragOptions = {
  onDown (props, monitor) {
    monitor.setData({
      initX: props.position.x,
      initY: props.position.y
    })
  },
  onDrag (props, monitor) {
    const offset = monitor.getDifferenceFromInitialOffset()

    props.position.set(
      monitor.data.initX + offset.x,
      monitor.data.initY + offset.y,
    )
  }
}

class Box extends React.Component {
  shouldComponentUpdate () {
    return false
  }
  componentDidMount () {
    this.dispose = this.props.position.addListener(() => {
      this.forceUpdate()
    })
  }

  componentWillUnmount () {
    this.dispose()
  }

  render () {
    const { driver, dragRef, position } = this.props

    const baseStyle = {
      ...styles.base,
      transform: `translate(${position.x}px, ${position.y}px)`
    }

    return (
      <div ref={dragRef}>
        <Panel style={baseStyle} title={driver.type}>
          <div style={{ display: 'flex' }}>
            <div style={{ flex: 1 }} />
            <Label label={driver.type} />
            <div style={{ flex: 1 }} />
            <Plug target={driver} />
          </div>
          {Object.values(driver.props).map(prop => {
            return prop.isArray
              ? (
                <IDListInput
                  key={prop.name}
                  prop={prop}
                />
              )
              : (
                <Option
                  key={prop.name}
                  prop={prop}
                />
              )
          })}
        </Panel>
      </div>
    )
  }
}

Box.propTypes = {
  driver: React.PropTypes.object.isRequired
}

const dragHoC = customDrag(dragOptions, connect => ({
  dragRef: connect.getDragRef()
}))

export default dragHoC(Box)
