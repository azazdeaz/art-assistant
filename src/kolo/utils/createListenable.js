// @flow
import pull from 'lodash/pull'

export default function createListenable (value) {
  const listeners = []

  function notifyListeners () {
    listeners.forEach(listener => listener(value))
  }

  function addListener (listener) {
    listeners.push(listener)
    return () => pull(listeners, listener)
  }

  return {
    get value () { return value },
    set value (newValue) {
      value = newValue
      notifyListeners()
    },
    addListener
  }
}
