// @flow

export default function getPositionID (target: any) {
  if (typeof target === 'string') {
    return target
  } else if (target.isDriver) {
    return `${target.id}->`
  } else if (target.isProp) {
    return `${target.idSource}-${target.name}`
  } else {
    throw new Error(`Unespected target ${target.toString()}`)
  }
}
