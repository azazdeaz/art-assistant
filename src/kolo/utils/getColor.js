// @flow
import gen from 'random-seed'

export default function getColor (seed: string) {
  const color = gen
    .create(seed)
    .intBetween(0, 0xffffff)
  return `#${('000000' + color.toString(16)).slice(-6)}`
}
