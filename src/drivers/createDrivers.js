// @flow

import createModel from '../twins/createModel'

export default function initDrivers (client) {
  const describe = type => createModel(client, type)

  describe('Layer')
    .prop('name', 'string', 'new Layer')
    .prop('source', 'string')
    .prop('transform', 'Transform', () => drivers.Transform.create())
    .seale()

  describe('UserMessage')
    .prop('text', 'string')
    .prop('processed', 'boolean', false)
    .action('setProcessed', (driver: Object) => () => {
      driver.processed = true
    })
    .seale()

  describe('BotMessage')
    .prop('text', 'string')
    .seale()

  describe('Doc')
    .prop('layers', '[Layer]', [])
    .prop('messages', '[Layer]', [])
    .prop('selectedLayer', 'Layer', null)
    .action('addLayer', (driver: Object) => (idLayer: string) => {
      driver.layers.push(idLayer)
    })
    .action('addMessage', (driver: Object) => (idMessage: string) => {
      driver.messages.push(idMessage)
    })
    .seale()

  describe('Transform')
    .prop('x', 'number', 0)
    .prop('y', 'number', 0)
    .prop('scaleX', 'number', 1)
    .prop('scaleY', 'number', 1)
    .prop('skewX', 'number', 0)
    .prop('skewY', 'number', 0)
    .prop('pivotX', 'number', 0)
    .prop('pivotY', 'number', 0)
    .prop('rotation', 'number', 0)
    .action('move', (driver: Object) => (x, y) => {
      driver.x = x
      driver.y = y
    })
    .seale()

  describe('Shape')
    .prop('width', 'number', 100)
    .prop('height', 'number', 100)
    .prop('color', 'number', 0xff3123)
    .prop('shape', 'string', 'rectangle')
    .prop('transform', 'Transform', () => drivers.Transform.create())
    .seale()

  describe('Sprite')
    .prop('source', 'string', null)
    .prop('transform', 'Transform', () => drivers.Transform.create())
    .seale()

  describe('Text')
    .prop('text', 'string', 'Text')
    .prop('fill', 'number', 0x000000)
    .prop('transform', 'Transform', () => drivers.Transform.create())
    .seale()

  return drivers
}
