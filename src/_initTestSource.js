import ts from './client'
const { Doc, Sprite, Shape, Text } = ts.drivers

global.init1 = () => {
  const doc = Doc.create()
  const rect = Shape.create()
  doc.addLayer(rect.id)
  const sprite = Sprite.create()
  sprite.source = `pe.png`
  sprite.transform.scaleX = sprite.transform.scaleY = 0.2
  sprite.transform.x = 123
  doc.addLayer(sprite.id)
  doc.addLayer(Text.create().id)

  global.d = {
    rect, doc, sprite
  }
}
// setTimeout(() => doc.selectedLayer = sprite.id, 500)
