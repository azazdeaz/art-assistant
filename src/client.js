// @flow

import createClient from './twins/createClient'
import createDrivers from './drivers/createDrivers'

const client = createClient()
client.connectServer()
client.drivers = createDrivers(client)
global.client = client
export default client
