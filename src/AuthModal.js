// @flow

import React, { Component } from 'react'
import { Modal, Input, Button } from 'antd'
import * as state from './state'

class AuthModal extends Component {
  constructor (props) {
    super(props)

    this.state = {
      username: ''
    }
  }
  handleChange (e) {
    this.setState({username: e.target.value})
  }
  render () {
    const {username} = this.state

    return (
      <Modal title='Auth' visible={!state.auth.token}
        onOk={this.handleOk}
        okText={'send'}
        closable={false}
        footer={
          <Button
            onClick={async () => {
              const response = await fetch('http://localhost:9000/login', {
                headers: {
                  'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify({username})
              })
              console.log(await response.json())
            }}
          >Send</Button>
        }
      >
        <Input placeholder='User name' onChange={e => this.handleChange(e)} />
      </Modal>
    )
  }
}

export default AuthModal
