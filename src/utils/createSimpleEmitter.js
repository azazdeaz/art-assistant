// @folw
import pull from 'lodash/pull'

export default function createSimpleEmitter () {
  const listeners = []

  function notify (...args) {
    listeners.forEach(listener => listener(...args))
  }

  function addListener (listener) {
    if (listeners.indexOf(listener) === -1) {
      listeners.push(listener)
    }
    return () => pull(listeners, listener)
  }

  return Object.freeze({
    notify,
    addListener,
    dispose: () => {
      listeners.length = 0
      Object.freeze(listeners)
    }
  })
}
