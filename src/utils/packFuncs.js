// @flow

export default function packFuncs (...funcs) {
  return () => funcs.forEach(fn => fn())
}
