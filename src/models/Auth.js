import {defineModel} from '../afflatus'

export default defineModel({
  type: 'Auth',
  simpleValues: {
    token: {type: 'string', canBeNull: true},
    username: {type: 'string', canBeNull: true}
  }
})
