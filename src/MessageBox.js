// @flow
import React from 'react'
import { Card, Input, Timeline, Icon } from 'antd'
import ts from './client'
import createListener from './twins/createListener'
import * as exec from './twins/exec'
const { UserMessage, BotMessage } = ts.drivers

const styles = {
  base: {
    position: 'absolute',
    right: 4,
    bottom: 4,
    width: 300,
    textAlign: 'left'
  }
}

export default class MessageBox extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      messages: [],
      draftInput: ''
    }

    ts.addListener(createListener(
      q => {
        const doc = q.find({type: 'Doc'})
        if (doc && doc.messages) {
          return doc.messages
        }
        return null
      },
      (messages) => {
        this.setState({messages})
      }
    ))
  }
  renderDot (message) {
    switch (message.type) {
      case UserMessage.type:
        return <Icon type='user' style={{ fontSize: '16px' }} />
      case BotMessage.type:
        return <Icon type='pause-circle-o' style={{ fontSize: '16px' }} />
      default:
        return null
    }
  }
  render () {
    const { messages, draftInput } = this.state

    return (
      <Card title='Messaging' bordered={false} style={styles.base}>
        <Timeline>
          {messages && messages
            .map(idMessage => ts.q.get(idMessage))
            .map(message => (
              <Timeline.Item
                key={message.id}
                dot={this.renderDot(message)}
              >
                {message.text}
              </Timeline.Item>
            ))
          }
        </Timeline>
        <div>
          <Input
            value={draftInput}
            onChange={e => this.setState({draftInput: e.target.value})}
            onPressEnter={() => {
              // debugger
              const msg = UserMessage.create(draftInput)
              const doc = ts.q.find({type: 'Doc'})
              ts.addAction(exec.set(doc.id, 'messages', [...doc.messages, msg.id]))
              this.setState({draftInput: ''})
            }}
          />
        </div>
      </Card>
    )
  }
}
