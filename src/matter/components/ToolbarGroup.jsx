import React from 'react'
import Radium from 'radium'
import assign from 'lodash/assign'
import has from 'lodash/has'
import MatterBasics from '../utils/MatterBasics'

export default Radium(MatterBasics(class Toolbar extends React.Component {
  render () {
    var {mod, style} = this.props

    mod = assign({}, mod)
    if (!has(mod, 'size')) {
      mod.size = this.getStyle('config').lineHeight
    }

    return <div
      {...this.getBasics()}
      style={this.getStyle('toolbarGroup', mod, style)}>
      {this.props.children}
    </div>
  }
}))
