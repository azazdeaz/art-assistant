import React from 'react'
import Radium from 'radium'
import MatterBasics from '../utils/MatterBasics'

export default Radium(MatterBasics(class Panel extends React.Component {
  render () {
    var {mod, style} = this.props

    return <div
      {...this.getBasics()}
      style={this.getStyle('panel', mod, style)}>

      {this.props.children}
    </div>
  }
}))
