import React from 'react'
import Radium from 'radium'
import assign from 'lodash/assign'
import MatterBasics from '../utils/MatterBasics'

export default Radium(MatterBasics(class Toolbar extends React.Component {
  static defaultProps = {
    direction: 'row'
  }

  render () {
    var {mod, style, direction, size} = this.props

    mod = assign({direction, size}, mod)

    return <div
      {...this.getBasics()}
      style={this.getStyle('toolbar', mod, style)}
      onClick={this.props.onClick}>
      {this.props.children}
    </div>
  }
}))
