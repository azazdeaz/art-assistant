// @flow
/* global PIXI */
import React from 'react'
import 'pixi.js'
import client from './client'
const xMap = new Map()
import createListener from './twins/createListener'
import * as exec from './twins/exec'
import packFuncs from './utils/packFuncs'

export function getComponentByID(id: string) {
  return xMap.get(id)
}
global.getComponentByID = getComponentByID // DEBUG

export default class DocRenderer extends React.Component {
  refHandler(node) {
    if (node) {
      const renderer = new PIXI.WebGLRenderer(800, 600, {
        antialias: true,
        resolution: 2,
        autoResize: true,
        backgroundColor: 0x3a3a3a,
      })
      node.appendChild(renderer.view)
      const stage = new PIXI.Stage()
      global.renderer = renderer
      global.stage = stage

      renderer.view.addEventListener('click', () => {
        console.log('out click')
      })

      function render() {
        renderer.render(stage)
        requestAnimationFrame(render)
      }
      render()

      client.addListener(createListener(
        q => q.find({type: 'Doc'}),
        root => {
          if (root) {
            const comp = createComponent('children', root.id)
            stage.addChild(comp.xView)
          }
        }
      ))
    }
  }
  render() {
    return <div ref={this.refHandler} />
  }
}

const components = {}

function createComponent(type, ...args) {
  // console.log('create component', type, args)
  if (!components[type]) {
    throw Error(`Unknown compoent type: "${type}"`)
  }
  const comp = components[type](...args)
  xMap.set(args[0], comp)
  return comp
}

function makeSelectable(xView, id) {
  xView.interactive = true
  xView.on('click', event => {
    event.stopPropagation()
    client.addAction(exec.set(client.q.find({type: 'Doc'}).id, 'selectedLayer', id))
  })
}

components.children = (idContainer) => {
  const xView = new PIXI.Container()
  const xChildren = new Map()

  const dispose = client.addListener(createListener(
    q => q.get(idContainer).layers.map(idChild => q.get(idChild)),
    children => {
      children.forEach(child => {
        if (!xChildren.has(child.id)) {
          xChildren.set(child.id, createComponent(child.type, child.id))
        }
      })
      // xView.removeChildren()
      if (children.length > 0) {
        xView.addChild(...children.map(({id}) => xChildren.get(id).xView))
      }
    }
  ))

  return {
    xView,
    dispose: () => {
      xChildren.forEach(({dispose}) => dispose())
      dispose()
    }
  }
}

const transformListener = (id, xView) => createListener(
  q => q.get(q.get(id).transform),
  transform => {
    xView.setTransform(
      transform.x,
      transform.y,
      transform.scaleX,
      transform.scaleY,
      transform.rotation,
      transform.skewX,
      transform.skewY,
      transform.pivotX,
      transform.pivotY,
    )
  }
)

const transformListener2 = (id, xView) => {
  const driver = client.getDriver(id)

  return packFuncs(
    driver.addListener(value => xView.x = value),
    driver.addListener(value => xView.y = value),
    driver.addListener(value => xView.scale.x = value),
    driver.addListener(value => xView.scale.y = value),
    driver.addListener(value => xView.rotation = value),
    driver.addListener(value => xView.skew.x = value),
    driver.addListener(value => xView.skew.y = value),
    driver.addListener(value => xView.pivot.x = value),
    driver.addListener(value => xView.pivot.y = value),
  )
}

components.Rectangle = (id) => {
  const xView = new PIXI.Graphics()
  makeSelectable(xView, id)

  xView.beginFill(0xFFFF00)
  xView.lineStyle(5, 0xFF0000)
  xView.drawRect(0, 0, 35, 35)

  const dispose = client.addListener(
    transformListener(id, xView),
  )

  return {
    xView,
    dispose,
  }
}

components.Shape = (id) => {
  const xView = new PIXI.Graphics()
  makeSelectable(xView, id)

  function draw() {
    const source = client.q.get(id)
    // xView.clear()
    xView.beginFill(source.color)
    // xView.lineStyle(5, 0xFF0000)
    switch (source.shape) {
      case 'circle': {
        xView.drawEllipse(
          source.width/2,
          source.height/2,
          source.width,
          source.height,
        )
        break
      }
      default: {
        xView.drawRect(0, 0, source.width, source.height)
      }
    }
  }
  draw()

  const dispose = client.addListener(
    transformListener(id, xView),
    ...[
      'width',
      'height',
      'color',
      'shape',
    ].map(key => createListener(
      q => q.get(id)[key],
      () => draw(),
    )),
  )

  return {
    xView,
    dispose,
  }
}

components.Text = (id) => {
  const driver = client.getDriver(id)
  const xView = new PIXI.Text('Valami')
  makeSelectable(xView, id)



  const dispose = packFuncs(
    driver.props.text.addListener(text => xView.text = text),
    driver.props.fill.addListener(fill => {
      xView.style = {
        fill: fill && `#${fill.toString(16)}`
      }
    }),
    transformListener2(driver.transform.id, xView),
  )

  return {
    xView,
    dispose,
  }
}

components.Sprite = (id) => {
  const xView = new PIXI.Sprite()
  makeSelectable(xView, id)

  const dispose = client.addListener(
    transformListener(id, xView),
    createListener(
      q => q.get(id).source,
      source => {
        xView.texture = source
          ? PIXI.Texture.fromImage(source, true)
          : null
      }
    )
  )

  return {
    xView,
    dispose,
  }
}
