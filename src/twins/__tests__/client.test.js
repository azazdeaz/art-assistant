// @flow
/* global it, expect, jest */

import createClient from '../createClient'
import * as exec from '../exec'

const initialState = [{ id: '1', type: 'Test', foo: 3 }]
const client = createClient()

it('creates a client object', () => {
  expect(typeof client).toBe('object')
})

it('can set state', () => {
  client.setState(initialState)
  expect(client._getState()).toEqual(initialState)
})

it('can get source by id', () => {
  client.setState(initialState)
  expect(client.q.get(initialState[0].id)).toEqual(initialState[0])
})

it('executes actions', () => {
  const itemThen = client.q.get(initialState[0].id)
  client.addAction(exec.set(initialState[0].id, 'foo', 4))
  const itemNow = client.q.get(initialState[0].id)
  expect(itemThen.foo).not.toBe(itemNow.foo)
})

it('calls and removes listeners', () => {
  const listener = jest.fn()
  let dispose = client.addListener(listener)
  client.setState(initialState)
  expect(listener).toHaveBeenCalledTimes(1)
  expect(listener).toHaveBeenCalledWith(client.q)
  dispose()
  client.setState(initialState)
  expect(listener).toHaveBeenCalledTimes(1)
})

it('call listener immediately if it is added with addListenerAndCall', () => {
  const listener = jest.fn()
  let dispose = client.addListenerAndCall(listener)
  expect(listener).toHaveBeenCalledTimes(1)
  expect(listener).toHaveBeenCalledWith(client.q)
  client.setState(initialState)
  expect(listener).toHaveBeenCalledTimes(2)
  dispose()
  client.setState(initialState)
  expect(listener).toHaveBeenCalledTimes(2)
})

it('calls and removes source listeners', () => {
  const listener = jest.fn()
  let dispose = client.addSourceListener(initialState[0].id, listener)
  client.addAction(exec.set(initialState[0].id, 'foo', 4))
  expect(listener).toHaveBeenCalledTimes(1)
  client.addAction(exec.insert({type: 'Test'}))
  expect(listener).toHaveBeenCalledTimes(1)
  dispose()
  client.addAction(exec.set(initialState[0].id, 'foo', 5))
  expect(listener).toHaveBeenCalledTimes(1)
})
