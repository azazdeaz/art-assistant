// @flow
/* global describe, it, expect, jest */

import createModel from '../createModel'
import createClient from '../createClient'
import * as exec from '../exec'

describe('Model & Driver', () => {
  const client = createClient()
  const model2 = createModel(client, 'Test2').seale()
  expect(model2.type).toBe('Test2')
  const model = createModel(client, 'Test')
    .prop('foo', 'string', 'baz')
    .prop('stringArray', '[string]')
    .prop('complex', 'Test2', () => model2.create())
    .prop('complexArray', '[Test2]')
    .action('makeFooBar', (driver) => () => {
      expect(typeof driver).toBe('object')
      driver.foo = 'bar'
    })
    .seale()

  expect(typeof model).toBe('object')
  const driver = model.create()
  it('creates a source in the client with .create()', () => {
    expect(typeof client.q.get(driver.id)).toBe('object')
  })
  expect(typeof driver).toBe('object')
  // sets the default value
  expect(driver.foo).toBe('baz')

  it('driver implements toString()', () => {
    expect(typeof driver.toString()).toBe('string')
  })
  it('prop implements toString()', () => {
    expect(typeof driver.props.foo.toString()).toBe('string')
  })

  it('updates the source with actions', () => {
    expect(typeof driver.makeFooBar).toBe('function')
    driver.makeFooBar()
    expect(client.q.get(driver.id).foo).toBe('bar')
    expect(driver.foo).toBe('bar')
  })

  it('reflects changes made directly to the client', () => {
    client.addAction(exec.set(driver.id, 'foo', 'qux'))
    expect(client.q.get(driver.id).foo).toBe('qux')
    expect(driver.foo).toBe('qux')
  })

  it('calls prop listeners', () => {
    expect(typeof driver.props.foo).toBe('object')
    const listener = jest.fn()
    const dispose = driver.props.foo.addListener(listener)
    driver.foo = 'fux'
    expect(listener).toHaveBeenCalledTimes(1)
    expect(listener).toHaveBeenCalledWith('fux')
    driver.foo = 'fux'
    expect(listener).toHaveBeenCalledTimes(1)
    client.addAction(exec.set(driver.id, 'foo', 'qux'))
    expect(listener).toHaveBeenCalledTimes(2)
    dispose()
    driver.foo = 'mux'
    expect(listener).toHaveBeenCalledTimes(2)
  })

  it('handles prop types', () => {
    expect(driver.props.foo.isComplex).toBe(false)
    expect(driver.props.foo.isArray).toBe(false)
    expect(driver.props.foo.is('string')).toBe(true)
    expect(driver.props.foo.is('number')).toBe(false)
    expect(driver.props.foo.is('number', 'string')).toBe(true)

    expect(driver.props.stringArray.isComplex).toBe(false)
    expect(driver.props.stringArray.isArray).toBe(true)

    expect(driver.props.complex.isComplex).toBe(true)
    expect(driver.props.complex.isArray).toBe(false)
    expect(driver.props.complex.is(model2.type)).toBe(true)

    expect(driver.props.complexArray.isComplex).toBe(true)
    expect(driver.props.complexArray.isArray).toBe(true)
  })

  it('can push into array through props', () => {
    const length = driver.complexArray.length
    driver.complexArray.push(model2.create())
    expect(driver.complexArray.length).toBe(length + 1)
  })

  it('replaces complex values with drivers', () => {
    expect(driver.complex.isDriver).toBe(true)
    expect(driver.complexArray[0].isDriver).toBe(true)
  })
})
