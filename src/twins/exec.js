// @flow
import immutable from 'object-path-immutable'
import get from 'lodash/get'

function findSourceIndex (state: Object[], _id: string): number {
  return state.findIndex(({id}) => id === _id)
}

function createID () {
  return `#${uuid()}`
}

export default function exec (state, action) {
  switch (action.type) {
    case 'insert':
      return [...state, action.params[0]]

    case 'set': {
      const [ id, path, value ] = action.params
      const index = findSourceIndex(state, id)
      return immutable.set(state, `${index}.${path}`, value)
    }

    case 'assign': {
      const [ id, path, value ] = action.params
      const index = findSourceIndex(state, id)
      return immutable.assign(state, `${index}.${path}`, value)
    }

    case 'push': {
      const [ id, path, value ] = action.params
      const index = findSourceIndex(state, id)
      return immutable.push(state, `${index}.${path}`, value)
    }

    case 'pull': {
      const [ id, path, value ] = action.params
      const index = findSourceIndex(state, id)
      const itemIndex = findSourceIndex(get(state, `${index}.${path}`), value)
      return immutable.del(state, `${index}.${path}.${itemIndex}`)
    }

    case 'splice': {
      const [ id, path, start, end, ...values ] = action.params
      const index = findSourceIndex(state, id)
      const arr = [ ...get(state, `${index}.${path}`) ]
      arr.splice(start, end, ...values)
      return immutable.set(state, `${index}.${path}`, arr)
    }

    case 'remove': {
      const [ id ] = action.params
      const index = findSourceIndex(state, id)
      return immutable.del(state, index)
    }

    default:
      throw new Error(`Unknown action type "${action.type}"`)
  }
}

import uuid from 'uuid/v4'

export const insert = (source) => ({
  type: 'insert',
  params: [{ ...source, id: createID() }]
})

export const set = (id, path, value) => ({
  type: 'set',
  params: [id, path, value]
})

export const assign = (id, path, value) => ({
  type: 'assign',
  params: [id, path, value]
})

export const push = (id, path, value) => ({
  type: 'push',
  params: [id, path, value]
})

export const pull = (id, path, value) => ({
  type: 'pull',
  params: [id, path, value]
})

export const splice = (id, path, start, end, ...values) => ({
  type: 'splice',
  params: [id, path, start, end, ...values]
})

export const remove = (id) => ({
  type: 'remove',
  params: [id]
})
