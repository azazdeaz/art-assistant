// @flow
import * as exec from './exec'
import createProp from './createProp'
import createListener from './createListener'

const createdDrivers = {}
global.drivers = createdDrivers

export default function createModel (client, type: string) {
  let isSealed = false
  const propSettings = []
  const actions = []

  function throwIfSealed () {
    if (isSealed) {
      throw new Error(`The model "${type}" is already sealed. (Called .create() after createModel())`)
    }
  }

  function throwIfNotSealed () {
    if (!isSealed) {
      throw new Error(`The model "${type}" isn't sealed. (Call .create() after createModel())`)
    }
  }

  function createSource (initial: Object = {}) {
    console.log('create source', type, initial, initial.hasOwnPorperty)
    throwIfNotSealed()
    const source = propSettings.reduce((acc, settings) => {
      if (settings.name in initial) {
        acc[settings.name] = initial[settings.name]
      } else {
        const def = typeof settings.def === 'function'
          ? settings.def()
          : settings.def
        acc[settings.name] = (typeof def === 'object' && def.isDriver)
          ? def.id
          : def
      }
      return acc
    }, {})
    const action = exec.insert({type, ...source})
    client.addAction(action)
    return createDriver(action.params[0].id)
  }

  function createDriver (idSource) {
    if (!/^#/.test(idSource)) {
      throw new Error() // debugger
    }

    throwIfNotSealed()
    if (createdDrivers[idSource]) {
      return createdDrivers[idSource]
    }
    const source = client.q.get(idSource)
    const props = Object.freeze(propSettings.reduce((acc, settings) => ({
      ...acc,
      [settings.name]: createProp(
        client,
        idSource,
        settings,
        source[settings.name],
      )
    }), {}))
    const addListener = listener => client.addListener(createListener(
      q => q.get(idSource),
      source => listener(source)
    ))

    const driver = {
      isDriver: true,
      addListener,
      type,
      id: idSource,
      props,
      dispose: () => {
        props.forEach(prop => props.dispose())
      },
      toString: () => `[${type} Driver] ${idSource}`
    }

    Object.keys(props).forEach(key => {
      const prop = props[key]
      Object.defineProperty(driver, prop.name, {
        get () { return prop.value },
        set (value) { prop.value = value }
      })
    })

    actions.forEach(({ name, fn }) => {
      driver[name] = (...args) => fn(driver)(...args)
    })

    Object.freeze(driver)
    createdDrivers[idSource] = driver
    return driver
  }

  const construct = {
    prop (name: string, type: string, def: ?any) {
      throwIfSealed()
      if (name === 'type' || name === 'id') {
        throw new Error('"type" and "id" are a restricted keys in sources')
      }
      propSettings.push({ name, type, def })
      return construct
    },
    action (name, fn) {
      throwIfSealed()
      actions.push({ name, fn })
      return construct
    },
    seale () {
      isSealed = true
      const model = Object.freeze({
        type,
        create: createSource,
        drive: createDriver,
        isDriver: true
      })
      client.addModel(model)
      return model
    }
  }

  return construct
}
