// @flow
import exec from './exec'
import io from 'socket.io-client'
import pullAll from 'lodash/pullAll'
import find from 'lodash/find'
import filter from 'lodash/filter'
import uuid from 'uuid/v4'

export default function createClient () {
  let socket
  let state = []
  let fixState = state
  let actions = []
  let fixActions = []
  const listeners = []
  const models = {}
  const sourceListeners:Map<string, Function[]> = new Map()

  const queryTools = {
    find: (predictate) => find(state, predictate),
    filter: (predictate) => filter(state, predictate),
    get: (id: string) => find(state, {id})
  }

  function addModel(model) {
    if (models[model.type]) {
      throw new Error(`A model with the type "${model.type}" is already added`)
    }
    models[model.type] = model
  }

  function getDriver(idSource: string) {
    const source = queryTools.get(idSource)
    if (!source) {
      throw new Error(`Can't find source with the id "${idSource}"`)
    }
    if (!models[source.type]) {
      throw new Error(`Can't find model with the type "${source.type}"`)
    }
    return models[source.type].drive(idSource)
  }

  function notifyListeners () {
    listeners.forEach(listener => listener(queryTools))
  }

  function notifySourceListeners (idSource) {
    if (sourceListeners.has(idSource)) {
      sourceListeners.get(idSource)
        .forEach(listener => listener(queryTools))
    }
  }

  function setState (initialState) {
    state = fixState = [...initialState]
    actions = fixActions = []
    notifyListeners()
  }

  function addFixActions (newFixActions, idOverrideUntil) {
    if (isDifferent(actions, fixActions, newFixActions)) {
      let unsyncedActions = null
      if (idOverrideUntil) {
        const idxOverrideUntil = actions.findIndex(({id}) => id === idOverrideUntil)
        unsyncedActions = actions.slice(idxOverrideUntil + 1)
      }

      state = fixState = execActions(fixState, newFixActions)
      actions = fixActions = [...fixActions, ...newFixActions]

      if (unsyncedActions) {
        console.log('add unsynced actions', unsyncedActions.length)
        state = execActions(state, unsyncedActions)
        actions = [...actions, ...unsyncedActions]
      }

      notifyListeners()
    } else {
      fixActions = [...fixActions, ...newFixActions]
      fixState = execActions(fixState, newFixActions)
    }
  }

  function connectServer () {
    socket = io('http://localhost:9000')
    socket.on('sendState', _state => setState(_state))
    socket.on('sendActions', ({actions, idOverrideUntil}) =>
      addFixActions(actions, idOverrideUntil)
    )
  }

  function addAction (action) {
    action = {...action, id: uuid()}
    actions = [...actions, action]
    state = exec(state, action)
    notifyListeners()
    notifySourceListeners(action.params[0])
    if (socket) {
      socket.emit('addAction', action)
    }
  }

  function addListener (..._listeners: Function[]) {
    listeners.push(..._listeners)
    return () => pullAll(listeners, _listeners)
  }

  function addListenerAndCall (..._listeners: Function[]) {
    _listeners.forEach(listener => listener(queryTools))
    return addListener(..._listeners)
  }

  function addSourceListener (idSource:string, ..._listeners: Function[]) {
    if (!sourceListeners.has(idSource)) {
      sourceListeners.set(idSource, [])
    }
    const listeners = sourceListeners.get(idSource)
    listeners.push(..._listeners)
    return () => pullAll(listeners, _listeners)
  }

  return {
    addModel,
    getDriver,
    setState,
    addAction,
    addListener,
    addListenerAndCall,
    addSourceListener,
    connectServer,
    q: queryTools,
    _getState: () => state,
    _getFixState: () => fixState,
    _getActions: () => actions,
    _getFixActions: () => fixActions
  }
}

function execActions (state, actions) {
  return actions.reduce((acc, action) => {
    return exec(acc, action)
  }, state)
}

function isDifferent (actions, fixActions, newFixActions) {
  const testActions = actions.slice(
    fixActions.length,
    fixActions.length + newFixActions.length,
  )
  if (newFixActions.length !== testActions.length) {
    return true
  }
  return !testActions.every(
    (a, index) => a.id === newFixActions[index].id
  )
}
