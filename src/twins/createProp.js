// @flow
import * as exec from './exec'
import createSimpleEmitter from '../utils/createSimpleEmitter'

function idToDriver (client: Object, value: any) {
  if (typeof value === 'string') {
    if (value[0] === '#') {
      return client.getDriver(value)
    }
    if (value.slice(0, 2) === '\\#') {
      return value.slice(1)
    }
  }
  return value
}

function driverToID (value) {
  if (typeof value === 'object' && value.isDriver) {
    return value.id
  }
  return value
}

const arrayProxyHandler = (client, idSource, name) => ({
  get: (target, property, receiver) => {
    switch (property) {
      case 'push':
        return (...args) => client.addAction(exec.push(idSource, name, ...args.map(driverToID)))
      case 'pull':
        return (...args) => client.addAction(exec.pull(idSource, name, ...args.map(driverToID)))
      case 'splice':
        return (...args) => client.addAction(exec.splice(idSource, name, ...args.map(driverToID)))
      case 'forEach':
        return fn => target.forEach((item, idx) => fn(idToDriver(client, item), idx, receiver))
      case 'shift':
      case 'unshift':
      case 'reverse':
      case 'slice':
        return () => { throw new Error(`"${property}" is unsupported`) }
      default:
        return idToDriver(client, target[property])
    }
  },
  set: () => {
    throw new Error(`Can't set`)
  }
})

export default function createProp (client, idSource, settings, initial) {
  const { type: rawType, name, def } = settings
  const isArray = /^\[.*?]$/.test(rawType)
  const type = /^\[?(.*?)]?$/.exec(rawType)[1]
  const isComplex = /^\[?[A-Z]/.test(rawType)
  const aph = isArray ? arrayProxyHandler(client, idSource, name) : null

  let value = initial !== undefined
    ? initial
    : typeof def === 'function'
    ? def()
    : def

  if (isArray && !(value instanceof Array)) {
    value = []
  }

  if (value !== initial) {
    client.addAction(exec.set(idSource, name, value))
  }

  const disposeSourceListener = client.addSourceListener(idSource, () => {
    const source = client.q.get(idSource)
    if (source && source[name] !== value) {
      value = source[name]
      emitter.notify(idToDriver(client, source[name]))
    }
  })

  const emitter = createSimpleEmitter()

  const prop = {
    get value () {
      if (isArray) {
        return new Proxy(value, aph)
      }
      return idToDriver(client, value)
    },
    set value (update) {
      update = driverToID(update)
      if (update === value) {
        return
      }
      value = update
      client.addAction(exec.set(idSource, name, value))
      emitter.notify(idToDriver(client, value))
    },
    type,
    isArray,
    isComplex,
    name,
    idSource,
    isProp: true,
    addListener: listener => emitter.addListener(listener),
    dispose: () => {
      emitter.dispose()
      disposeSourceListener()
    },
    is: (...types) => types.indexOf(type) !== -1,
    toString: () => `[Prop:${type}] ${name}`
  }

  return Object.freeze(prop)
}
