// @flows

export default function (query: Function, listener: Function) {
  let prevValue = null

  return function (queryTools) {
    const value = query(queryTools)
    if (prevValue !== value) {
      prevValue = value
      listener(value)
    }
  }
}
