// @flow

import exec from './exec'

export default function createServer (initial = []) {
  let state = [...initial]
  const actions = []
  let connectionCounter = 0

  function createConnection (
    sendState: (state: Object) => {},
    dispatchActions: (actions: Object[], idLastAction: string) => {},
  ) {
    const idConnection = connectionCounter++
    let lastSyncedAt = actions.length
    let idLastAction = null
    let disposed = false

    async function sync () {
      while (!disposed) {
        const currentPos = actions.length
        if (lastSyncedAt < currentPos) {
          const _idLastAction = idLastAction
          idLastAction = null
          console.log(`#${idConnection} sync`, lastSyncedAt, currentPos, _idLastAction)
          await dispatchActions(actions.slice(lastSyncedAt), _idLastAction)
          lastSyncedAt = currentPos
        }
        await new Promise(resolve => setTimeout(() => resolve(), 66))
      }
    }

    function addAction (action) {
      try {
        state = exec(state, action)
        idLastAction = action.id
        actions.push(action)
        console.log('addAction', action.id, action.type, actions.length)
      } catch (error) {

      }
    }

    const connection = {
      addAction,
      dispose: () => disposed = true
    }

    sendState(state)
    sync()

    return connection
  }

  return {
    createConnection
  }
}
