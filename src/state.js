import { createModel } from './afflatus'

import Auth from './models/Auth'

export const auth = createModel(Auth)
