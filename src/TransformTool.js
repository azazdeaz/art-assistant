// @flow
/* global PIXI */
import React from 'react'
import { Transhand } from 'transhand'
import client from './client'
import { getComponentByID } from './DocRenderer'
import createListener from './twins/createListener'
import * as exec from './twins/exec'

const keyMap = {
  tx: 'x',
  ty: 'y',
  sx: 'scaleX',
  sy: 'scaleY',
  rz: 'rotation',
  ox: 'pivotX',
  oy: 'pivotY'
}

export default class TarnsformTool extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      transform: null,
      rect: null
    }
  }
  componentWillMount () {
    client.addListener(createListener(
      q => {
        const doc = q.find({type: 'Doc'})
        if (doc && doc.selectedLayer) {
          return q.get(doc.selectedLayer)
        }
        return null
      },
      layer => {
        if (layer) {
          const displayObject = getComponentByID(layer.id)
          if (!displayObject) {
            return
          }
          const { xView } = displayObject
          const x = xView.position.x
          const y = xView.position.y
          const scaleX = xView.scale.x
          const scaleY = xView.scale.y
          const rotation = xView.rotation
          xView.position.x = 0
          xView.position.y = 0
          xView.scale.x = 1
          xView.scale.y = 1
          xView.rotation = 0

          const rect = {
            x: xView.x,
            y: xView.y,
            w: xView.width,
            h: xView.height
          }

          xView.position.x = x
          xView.position.y = y
          xView.scale.x = scaleX
          xView.scale.y = scaleY
          xView.rotation = rotation

          this.setState({ rect })
        }
      }
    ),
    createListener(
      q => {
        const doc = q.find({type: 'Doc'})
        if (doc && doc.selectedLayer) {
          return client.createDriver(doc.selectedLayer).transform
        }
        return null
      },
      transform => {
        this.setState({transform})
      }
    ))
  }
  render () {
    const { rect, transform } = this.state

    if (!rect || !transform) {
      return <div hidden />
    }

    const t = {
      tx: transform.x, ty: transform.y,
      sx: transform.scaleX, sy: transform.scaleY,
      rz: transform.rotation,
      ox: transform.pivotX, oy: transform.pivotY
    }

    return (
      <Transhand
        transform={t}
        rect={rect}
        onChange={change => {
          Object.keys(change).forEach(key => {
            transform[keyMap[key]] = change[key]
          })
        }}
      />
    )
  }
}
